FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install -y apache2
CMD ["apachectl","-D","FOREGROUND"]
RUN a2enmod rewrite
RUN echo 'ServerName cicd.maurya.me' >> /etc/apache2/apache2.conf

RUN rm -rf /var/www/html/*
ADD . /var/www/html
WORKDIR /var/www/html

RUN service apache2 restart
EXPOSE 80
